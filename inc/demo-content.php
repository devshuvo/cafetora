<?php
function cafetora_import_files() {
  return array(
    array(
      'import_file_name'             => 'Homepage - 1 ( Light )',
      'categories'                   => array( 'Light Versions' ),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demo-content/homepage-1/light/demo-content.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demo-content/homepage-1/light/widgets.wie',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'inc/demo-content/homepage-1/light/customizer.dat',
      'local_import_redux'           => array(
        array(
          'file_path'   => trailingslashit( get_template_directory() ) . 'inc/demo-content/homepage-1/light/redux.json',
          'option_name' => 'ct_option',
        ),
      ),
      'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ) . 'inc/demo-content/homepage-1/light/white-preview.png',
      'import_notice'                => __( 'After you import this demo, you have to assign Static Homepage from <strong>Settings>Reading</strong>', 'cafetora' ),
      'preview_url'                  => 'http://cafetora.our-server.xyz',
    ),
    array(
      'import_file_name'             => 'Homepage - 1 ( Dark )',
      'categories'                   => array( 'Dark Versions' ),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demo-content/homepage-1/dark/demo-content.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demo-content/homepage-1/dark/widgets.wie',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'inc/demo-content/homepage-1/dark/customizer.dat',
      'local_import_redux'           => array(
        array(
          'file_path'   => trailingslashit( get_template_directory() ) . 'inc/demo-content/homepage-1/dark/redux.json',
          'option_name' => 'ct_option',
        ),
      ),
      'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ) . 'inc/demo-content/homepage-1/dark/black-preview.png',
      'import_notice'                => __( 'After you import this demo, you have to assign Static Homepage from <strong>Settings>Reading</strong>', 'cafetora' ),
      'preview_url'                  => 'http://cafetora.our-server.xyz/dark-homepage',
    ),
  );
}
add_filter( 'pt-ocdi/import_files', 'cafetora_import_files' );