<?php
/**
 *  Custom CSS function 
 */
if( !function_exists( 'cafetora_custom_css' ) ){
    function cafetora_custom_css(){
        global $ct_option;
        $output = '';

        if( $ct_option['text_font'] != '' ) :
            $output .= '
            body{
                font-family: '.$ct_option["text_font"].';
            }
            ';
        endif;

        if( $ct_option['heading_font'] != '' ) :
            $output .= '
            h1, h2, h3, h4, h5, h6 {
                font-family: '.$ct_option["heading_font"].';
            }
            ';
        endif;

        if( $ct_option['pbtn_color'] != '' ) :
            $output .= '
            .cafetora-large-btn:not(.white-btn),
            .cafetora-medium-btn:not(.white-btn),
            .cafetora-small-btn:not(.white-btn),
            .cafetora-fullwidth-btn, .wpcf7-submit,
            .white-btn:before{
                background-color: '.$ct_option["pbtn_color"].';
            }
            ';
        endif;

        if( $ct_option['pbtn_color_hover'] != '' ) :
            $output .= '
            .cafetora-large-btn:not(.white-btn):before,
            .cafetora-medium-btn:not(.white-btn):before,
            .cafetora-small-btn:not(.white-btn):before,
            .white-btn{
                background-color: '.$ct_option["pbtn_color_hover"].';
            }
            ';
        endif;

        if( $ct_option['title_sep_color'] != '' ) :
            $output .= '
            .section-border{
                background-color: '.$ct_option["title_sep_color"].';
            }
            ';
        endif;

        if( $ct_option['header_bar_bg'] != '' ) :
            $output .= '
            .header-area{
                background-color: '.$ct_option["header_bar_bg"].';
            }
            ';
        endif;

        if( $ct_option['header_bar_border'] != '' ) :
            $output .= '
            .header-area{
                border-color: '.$ct_option["header_bar_border"].';
            }
            ';
        endif;

        if( $ct_option['sticky_header_bar_bg'] != '' ) :
            $output .= '
            .header-area.sticky{
                background-color: '.$ct_option["sticky_header_bar_bg"].';
            }
            ';
        endif;

        if( $ct_option['menu_text_color'] != '' ) :
            $output .= '
            .main-menu li a{
                color: '.$ct_option["menu_text_color"].';
            }
            ';
        endif;

        if( $ct_option['menu_text_color_hover'] != '' ) :
            $output .= '
            .main-menu li:hover > a{
                color: '.$ct_option["menu_text_color_hover"].';
            }
            .main-menu li:before{
                background-color: '.$ct_option["menu_text_color_hover"].';
            }
            ';
        endif;

        if( $ct_option['sidebar_heading_color'] != '' ) :
            $output .= '
            h4.widget-title{
                color: '.$ct_option["sidebar_heading_color"].';
            }
            ';
        endif;

        if( $ct_option['sidebar_text_color'] != '' ) :
            $output .= '
            .sidebar .widget p{
                color: '.$ct_option["sidebar_text_color"].';
            }
            ';
        endif;

        if( $ct_option['sidebar_link_color'] != '' ) :
            $output .= '
            .sidebar .widget a,
            .featured-blog h4{
                color: '.$ct_option["sidebar_link_color"].';
            }
            ';
        endif;

        if( $ct_option['sidebar_link_color_hover'] != '' ) :
            $output .= '
            .sidebar .widget a:hover,
            .featured-blog:hover h4{
                color: '.$ct_option["sidebar_link_color_hover"].';
            }
            ';
        endif;

        if( $ct_option['footer_top_bg'] != '' ) :
            $output .= '
            .footer-top-area{
                background-color: '.$ct_option["footer_top_bg"].';
            }
            ';
        endif;

        if( $ct_option['footer_top_bgimg']['url'] != '' ) :
            $output .= '
            .footer-top-area{
                background-image: url('.$ct_option["footer_top_bgimg"]["url"].');
            }
            ';
        endif;

        if( $ct_option['footer_top_h_color'] != '' ) :
            $output .= '
            .footer-widget .widget-title{
                color: '.$ct_option["footer_top_h_color"].';
            }
            ';
        endif;

        if( $ct_option['footer_top_p_color'] != '' ) :
            $output .= '
            .footer-widget p{
                color: '.$ct_option["footer_top_p_color"].';
            }
            ';
        endif;

        if( $ct_option['footer_top_link_color'] != '' ) :
            $output .= '
            .footer-top-area a:not([href]),
            .footer-top-area a,
            .footer-social i.fa{
                color: '.$ct_option["footer_top_link_color"].';
            }
            ';
        endif;

        if( $ct_option['footer_top_link_color_hover'] != '' ) :
            $output .= '
            .footer-top-area a:not([href]):hover,
            .footer-top-area a:hover,
            .footer-social i.fa:hover{
                color: '.$ct_option["footer_top_link_color_hover"].';
            }
            ';
        endif;

        if( $ct_option['footer_btm_bg'] != '' ) :
            $output .= '
            .footer-copyright-area{
                background-color: '.$ct_option["footer_btm_bg"].';
            }
            ';
        endif;

        if( $ct_option['footer_btm_color'] != '' ) :
            $output .= '
            .footer-copyright-area{
                color: '.$ct_option["footer_btm_color"].';
            }
            ';
        endif;

        if( $ct_option['footer_btmlink_color'] != '' ) :
            $output .= '
            .footer-copyright-area a{
                color: '.$ct_option["footer_btmlink_color"].';
            }
            ';
        endif;

        if( $ct_option['footer_btmlink_color_hover'] != '' ) :
            $output .= '
            .footer-copyright-area a:hover{
                color: '.$ct_option["footer_btmlink_color_hover"].';
            }
            ';
        endif;

        if( $ct_option['footer_border_color'] != '' ) :
            $output .= '
            .footer-top-area{
                border-color: '.$ct_option["footer_border_color"].';
            }
            ';
        endif;

        //Custom css
        if( $ct_option['ct-custom-css'] != '' ) :
            $output .= $ct_option['ct-custom-css'];
        endif;

        wp_add_inline_style( 'cafetora-style', cafetora_css_compress( $output ) );
    }
}
add_action( 'wp_enqueue_scripts', 'cafetora_custom_css', 200 );
