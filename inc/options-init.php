<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "ct_option";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Theme Options', 'cafetora' ),
        'page_title'           => __( 'Cafetora Theme Options', 'cafetora' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => 'cafetora_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('General', 'cafetora') ,
        'icon'      => '',
        'desc'      => esc_html__('This is basic section where you can set up main settings for your website.', 'cafetora'),
        'fields' => array(
            array(
                'id'       => 'preloader',
                'type'     => 'switch',
                'title'    => __( 'Preloader', 'cafetora' ),
                'default'  => 1,
                'on'       => 'Enabled',
                'off'      => 'Disabled',
            ),
            array(
                'id'       => 'scroll_to_top',
                'type'     => 'switch',
                'title'    => __( 'Scroll To Top', 'cafetora' ),
                'default'  => 1,
                'on'       => 'Enabled',
                'off'      => 'Disabled',
            ),
            array(
                'id'       => 'sticky_header',
                'type'     => 'switch',
                'title'    => __( 'Sticky Header', 'cafetora' ),
                'default'  => 0,
                'on'       => 'Enabled',
                'off'      => 'Disabled',
            )            
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('Sidebar', 'cafetora') ,
        'icon'      => '',
        'desc'      => esc_html__('Set up Sidebar for your website.', 'cafetora'),
        'fields' => array(        
            array(
                'id'       => 'post_sidebar',
                'type'     => 'image_select',
                'title'    => __( 'Post Sidebar', 'cafetora' ),
                'options'  => array(
                    '1' => array(
                        'alt' => 'No Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    '2' => array(
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    '3' => array(
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    )
                ),
                'default'  => '3'
            ),
            array(
                'id'       => 'page_sidebar',
                'type'     => 'image_select',
                'title'    => __( 'Page Sidebar', 'cafetora' ),
                'options'  => array(
                    '1' => array(
                        'alt' => 'No Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/1col.png'
                    ),
                    '2' => array(
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    '3' => array(
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    )
                ),
                'default'  => '1'
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('Footer', 'cafetora') ,
        'icon'      => '',
        'desc'      => esc_html__('Footer settings.', 'cafetora'),
        'fields' => array(
            array(
                'id'        => 'show_footer_top',
                'type'      => 'select',
                'options'   => array(
                    'yes'        => esc_html__( 'Yes', 'cafetora' ),
                    'no'        => esc_html__( 'No', 'cafetora' ),
                ),
                'title'     => esc_html__('Enable Footer Widgets', 'cafetora') ,
                'desc'      => esc_html__('Enable or disable footer widget section.', 'cafetora'),
                'default'   => 'yes'
            ),
            array(
                'id'        => 'footer_left_text',
                'type'      => 'text',
                'title'     => esc_html__('Footer Left Text', 'cafetora') ,
                'desc'      => esc_html__('Input text which will be visible at the bottom-left of the page.', 'cafetora'),
                'default'      => __('© Copyright 2018 Cafetora | All Rights Reserved', 'cafetora'),
            ),
            array(
                'id'        => 'footer_right_text',
                'type'      => 'text',
                'title'     => esc_html__('Footer Right Text', 'cafetora') ,
                'desc'      => esc_html__('Input text which will be visible at the bottom-left of the page.', 'cafetora'),
                'default'      => __('Developed By <a href="https://a-web.org" target="_blank">A-WEB</a>', 'cafetora'),
            ),
        )
    ) );


    Redux::setSection( $opt_name, array(
        'title'     => esc_html__('Appearance', 'cafetora') ,
        'icon'      => '',
        'desc'      => esc_html__('Set up the looks.', 'cafetora'),
        'fields' => array(
            array(
                'id'            => 'pbtn_color',
                'type'          => 'color',
                'title'         => esc_html__('Primary Button Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ), 
            array(
                'id'            => 'pbtn_color_hover',
                'type'          => 'color',
                'title'         => esc_html__('Primary Button Color On Hover', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'title_sep_color',
                'type'          => 'color',
                'title'         => esc_html__('Title Seperator Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'       => 'ct-custom-css',
                'type'     => 'ace_editor',
                'title'    => __( 'Custom CSS', 'cafetora' ),
                'subtitle' => __( 'If you want to make extra CSS then you can do it from here', 'cafetora' ),
                'mode'   => 'css',
                'theme'    => 'monokai',
                'default'  => ""
            ),
        )
    ) );



    Redux::setSection( $opt_name, array(
        'title'      => __( 'Typography', 'cafetora' ),
        'subsection' => true,
        'fields'     => array(
            array(
                'id'            => 'heading_font',
                'type'          => 'select',
                'title'         => esc_html__('Heading Font', 'cafetora'),
                'subtitle'          => esc_html__('Select font for the all Heading text.', 'cafetora'),
                'options'       => cafetora_all_google_fonts(),
                'default'       => 'Alegreya'
            ),
            array(
                'id'            => 'text_font',
                'type'          => 'select',
                'title'         => esc_html__('Text Font', 'cafetora'),
                'subtitle'          => esc_html__('Select font for the all regular text.', 'cafetora'),
                'options'       => cafetora_all_google_fonts(),
                'default'       => 'Lora'
            ),
        )
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Header', 'cafetora' ),
        'subsection' => true,
        'fields'     => array(
            array(
                'id'            => 'header_bar_bg',
                'type'          => 'color',
                'title'         => esc_html__('Header Background Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'menu_text_color',
                'type'          => 'color',
                'title'         => esc_html__('Header Menu Text Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'menu_text_color_hover',
                'type'          => 'color',
                'title'         => esc_html__('Header Menu Text Color on Hover', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'sticky_header_bar_bg',
                'type'          => 'color',
                'title'         => esc_html__('Sticky Header Background Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'header_bar_border',
                'type'          => 'color',
                'title'         => esc_html__('Header Border Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Sidebar', 'cafetora' ),
        'subsection' => true,
        'fields'     => array(
            array(
                'id'            => 'sidebar_heading_color',
                'type'          => 'color',
                'title'         => esc_html__('Heading Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'sidebar_text_color',
                'type'          => 'color',
                'title'         => esc_html__('Widget Text Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'sidebar_link_color',
                'type'          => 'color',
                'title'         => esc_html__('Links Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'sidebar_link_color_hover',
                'type'          => 'color',
                'title'         => esc_html__('Links Hover Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Footer Top', 'cafetora' ),
        'subsection' => true,
        'fields'     => array(
            array(
                'id'            => 'footer_top_bg',
                'type'          => 'color',
                'title'         => esc_html__('Background Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'footer_top_bgimg',
                'title'         => esc_html__('Background Image', 'cafetora'),
                'type'          => 'media',
                'desc'          => esc_html__('Upload Footer Top Background Image', 'cafetora'),
                'default'       => array(
                    'url'   =>''
                ),
            ),
            array(
                'id'            => 'footer_top_h_color',
                'type'          => 'color',
                'title'         => esc_html__('Widget Heading Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'footer_top_p_color',
                'type'          => 'color',
                'title'         => esc_html__('Widget Text Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'footer_top_link_color',
                'type'          => 'color',
                'title'         => esc_html__('Links Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'footer_top_link_color_hover',
                'type'          => 'color',
                'title'         => esc_html__('Links Hover Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
        )
    ) );
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Footer Bottom', 'cafetora' ),
        'subsection' => true,
        'fields'     => array(
            array(
                'id'            => 'footer_btm_bg',
                'type'          => 'color',
                'title'         => esc_html__('Background Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'footer_btm_color',
                'type'          => 'color',
                'title'         => esc_html__('Text Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'footer_btmlink_color',
                'type'          => 'color',
                'title'         => esc_html__('Link Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'footer_btmlink_color_hover',
                'type'          => 'color',
                'title'         => esc_html__('Link Hover Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
            array(
                'id'            => 'footer_border_color',
                'type'          => 'color',
                'title'         => esc_html__('Footer Border Color', 'cafetora'),
                'transparent'   => false,
                'default'       => ''
            ),
        )
    ) );

    /*
     * <--- END SECTIONS
     */
