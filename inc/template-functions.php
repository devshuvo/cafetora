<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Cafetora
 */

/**
 * Cafetora Tag List
 */
function cafetora_tags_list(){
    $tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'cafetora' ) );
    if ( $tags_list && is_single() ) {
        $tag_output = '<li><i class="fa fa-tags"></i>';         
        $tag_output .= $tags_list;          
        $tag_output .= '</li>';

        return $tag_output;
    }
}

/**
 * Cafetora Post Meta
 */
if ( ! function_exists( 'cafetora_post_meta' ) ) :

    function cafetora_post_meta() {

        $categories = get_the_category();
        $separator = ', ';
        
        $cat_output = '<li><i class="fa fa-thumb-tack"></i>';
        if ( ! empty( $categories ) ) {
            foreach( $categories as $category ) {
                $cat_output .= esc_html( $category->name )  . $separator;
            }           
        }
        $cat_output .= '</li>';

        echo '<div class="post-meta">
            <ul>
                <li><i class="fa fa-calendar-o"></i> '.get_the_date().'</li>
                '.trim( $cat_output, $separator ).'
                '.cafetora_tags_list().'
            </ul>
        </div>';
    }
endif;

/**
 * Cafetora Comment Template
 */
function cafetora_comment_callback($comment, $args, $depth) {
    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }?>
    <<?php echo esc_attr($tag); ?> <?php comment_class( array('comment-item', empty( $args['has_children'] ) ? '' : 'parent') ); ?> id="comment-<?php comment_ID() ?>"><?php 
    if ( 'div' != $args['style'] ) { ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body row"><?php
    } ?>
        <div class="comment-author col-md-1"><?php 
            if ( $args['avatar_size'] != 0 ) {
                echo get_avatar( $comment, $args['avatar_size'] ); 
            } 
            ?>
        </div>
        <div class="col-md-11">
            <div class="comment-meta comment-metadata">
                <?php 
                printf( __( '<h4 class="name">%s</h4>', 'cafetora' ), get_comment_author_link() );
                if ( $comment->comment_approved == '0' ) { ?>
                    <p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.','cafetora' ); ?></p><?php 
                } ?>
                <a class="comment-time" href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>"><?php
                    /* translators: 1: date, 2: time */
                    printf( 
                        __('%1$s at %2$s', 'cafetora'), 
                        get_comment_date(),  
                        get_comment_time() 
                    ); ?>
                </a><?php 
                edit_comment_link( __( '(Edit)','cafetora' ), '  ', '' ); ?>
            </div>
            <div class="comment-content">
                <?php comment_text(); ?>                    
            </div>
            <div class="reply"><?php 
                    comment_reply_link( 
                        array_merge( 
                            $args, 
                            array( 
                                'add_below' => $add_below, 
                                'depth'     => $depth, 
                                'max_depth' => $args['max_depth'] 
                            ) 
                        ) 
                    ); ?>
            </div>
        </div><?php 
    if ( 'div' != $args['style'] ) : ?>
        </div><?php 
    endif;
}

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function cafetora_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'cafetora_pingback_header' );

/**
 * Cafetoa Logo
 */
function cafetora_custom_logo() {
    
	if( has_custom_logo() ){
        the_custom_logo();        
     } else{ ?>
        <div class="logo-text">
        	<?php bloginfo( 'name' ); ?>        	
        	<span><?php echo get_bloginfo( 'description', 'display' ); ?></span>
        </div><?php 
    }
}

/**
 * Preloader
 */
function cafetora_preloader() { 
    global $ct_option;    
    if ( $ct_option['preloader'] == 0 )
        return;
        ?>
        <div class="cafetora-site-preloader-wrap">
            <div class="spinner"></div>
        </div>
    <?php 
}
add_action( 'cafetora_header', 'cafetora_preloader', 5 );

/**
 * Header Intro
 */
function cafetora_header_intro() {
    if( ! class_exists('ACF') )
        return;

	$hero_type = get_field('header_type');
	$hero = get_field('intro_text_header');

	if( $hero_type == 'Intro Text' ): ?>
	    <!-- Intro Area Start -->
	    <div class="intro cafetora-overlay" style="background-image: url(<?php echo esc_url( $hero['background'] ); ?>);">
	     <div class="content">
	         <div class="container-fluid">
	           <div class="row">
	               <div class="col-md-7">
	                   <h1 class="intro-heading">
	                  <span class="text-wrapper">
	                    <span class="line line1"></span>
	                    <span class="letters letters-left"><?php echo esc_attr( $hero['left_text'] ); ?></span>
	                    <span class="letters ampersand"><?php echo esc_attr( $hero['center_text'] ); ?></span>
	                    <span class="letters letters-right"><?php echo esc_attr( $hero['right_text'] ); ?></span>
	                    <span class="line line2"></span>
	                  </span>
	                </h1>
	                <p><?php echo esc_attr( $hero['description'] ); ?></p>
	                <a class="btn cafetora-medium-btn" href="<?php echo esc_url( $hero['btn_one_link'] ); ?>"><?php echo esc_attr( $hero['btn_one_text'] ); ?></a> 
	                <a class="btn cafetora-medium-btn white-btn" href="<?php echo esc_url( $hero['btn_two_link'] ); ?>"><?php echo esc_attr( $hero['btn_two_text'] ); ?></a>
	               </div>
	           </div>
	         </div>
	     </div>
	    </div>
	    <!-- Intro Area End -->
    <?php endif;
}
add_action( 'cafetora_header', 'cafetora_header_intro', 10 );

/**
 * Header Menu
 */
function cafetora_header_menu() { 
	?>
    <!-- Header Area Start -->
    <div class="header-area" id="cafetoraHeader">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6 col-sm-6 col-md-12 col-lg-3">
                    <div class="logo">                        
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">                            
                            <h1>
                            	<?php cafetora_custom_logo(); ?>                            	
                            </h1>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-12 col-lg-9">
                	
                    <div class="main-menu">
                    <?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'navigation',
						'container' => 'ul',
					) );
					?>
                    </div>
                    <!-- Mobile Menu Area Start -->
                        <div class="responsive-menu-wrap"></div>
                    <!-- Mobile Menu Area End -->
                </div>
            </div>
        </div>
        <!-- Header Area Start -->
    </div>
    <!-- Main Header Area End -->
    <?php	
}
add_action( 'cafetora_header', 'cafetora_header_menu', 15 );

/**
 * Footer Widgets
 */
function cafetora_footer_widgets() { 
	global $ct_option;

    if ( ! is_active_sidebar( 'footer-widget-1' ) && ! is_active_sidebar( 'footer-widget-2' ) && ! is_active_sidebar( 'footer-widget-3' ) )
        return;
    if( $ct_option['show_footer_top'] == 'no' )
    	return;
	?>
    <!-- Footer Top Area Start -->
    <div class="footer-top-area section-padding">
        <div class="container">
            <div class="row">
                <?php 
                if ( is_active_sidebar( 'footer-widget-1' ) ) {
                    echo '<div class="col-md-4">';
                        dynamic_sidebar( 'footer-widget-1' );
                    echo '</div>';
                         
                }
                if ( is_active_sidebar( 'footer-widget-2' ) ) {
                    echo '<div class="col-md-4">';
                        dynamic_sidebar( 'footer-widget-2' );
                    echo '</div>';
                         
                }
                if ( is_active_sidebar( 'footer-widget-3' ) ) {
                    echo '<div class="col-md-4">';
                        dynamic_sidebar( 'footer-widget-3' );
                    echo '</div>';
                         
                }
                ?>
            </div>
        </div>
    </div>
    <!-- Footer Top Area End -->
    <?php	
}
add_action( 'cafetora_footer', 'cafetora_footer_widgets', 5 );

/**
 * Footer Copyrights
 */
function cafetora_footer_copyright_area() {
	global $ct_option;
	?>
    <!-- Footer Copyright Area Start -->
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
	                <?php 
	                if( $ct_option['footer_left_text'] ){
                        echo wp_kses( $ct_option['footer_left_text'],array(
                            'a' => array(
                                'href' => array(),
                                'target' => array(),
                                'title' => array()
                            ),
                            'strong' => array(),
                        ) );
	                }else{
	                	echo esc_attr('&copy; Copyright 2018 Cafetora | All Rights Reserved','cafetora');
	                }
	                ?>                    
                </div>
                <div class="col-md-6 foot-credit">
	                <?php 
	                if( $ct_option['footer_right_text'] ){
                        echo wp_kses( $ct_option['footer_right_text'],array(
                            'a' => array(
                                'href' => array(),
                                'target' => array(),
                                'title' => array()
                            ),
                            'strong' => array(),
                        ) );
	                }else{
	                	echo esc_html('Developed By A-WEB','cafetora');
	                }
	                ?>                    
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Copyright Area End -->
    <?php	
}
add_action( 'cafetora_footer', 'cafetora_footer_copyright_area', 10 );

/**
 * Scroll to top
 */
function cafetora_scroll_to_top() { 
    global $ct_option;

    if( $ct_option['scroll_to_top'] ): ?>
        <a data-scroll href="" class="go-top"><?php echo esc_attr('Top','cafetora'); ?> <i class="fa fa-long-arrow-up"></i></a>
    <?php endif;
}
add_action( 'cafetora_footer', 'cafetora_scroll_to_top', 15 );

/**
 *  Customiser Tweaks 
 */
function cafetora_customize_tweak( $wp_customize ) {
    $wp_customize->add_section( 'colors' , array(
        'title'      => __( 'Background Color', 'cafetora' ),
        'priority'   => 30,
    ) );
}
add_action( 'customize_register', 'cafetora_customize_tweak' );

/**
 *  Compress CSS
 */
function cafetora_css_compress( $css ) {  
    $css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
    // backup values within single or double quotes
    preg_match_all('/(\'[^\']*?\'|"[^"]*?")/ims', $css, $hit, PREG_PATTERN_ORDER);
    for ($i=0; $i < count($hit[1]); $i++) {
      $css = str_replace($hit[1][$i], '##########' . $i . '##########', $css);
    }
    // remove traling semicolon of selector's last property
    $css = preg_replace('/;[\s\r\n\t]*?}[\s\r\n\t]*/ims', "}\r\n", $css);
    // remove any whitespace between semicolon and property-name
    $css = preg_replace('/;[\s\r\n\t]*?([\r\n]?[^\s\r\n\t])/ims', ';$1', $css);
    // remove any whitespace surrounding property-colon
    $css = preg_replace('/[\s\r\n\t]*:[\s\r\n\t]*?([^\s\r\n\t])/ims', ':$1', $css);
    // remove any whitespace surrounding selector-comma
    $css = preg_replace('/[\s\r\n\t]*,[\s\r\n\t]*?([^\s\r\n\t])/ims', ',$1', $css);
    // remove any whitespace surrounding opening parenthesis
    $css = preg_replace('/[\s\r\n\t]*{[\s\r\n\t]*?([^\s\r\n\t])/ims', '{$1', $css);
    // remove any whitespace between numbers and units
    $css = preg_replace('/([\d\.]+)[\s\r\n\t]+(px|em|pt|%)/ims', '$1$2', $css);
    // shorten zero-values
    $css = preg_replace('/([^\d\.]0)(px|em|pt|%)/ims', '$1', $css);
    // constrain multiple whitespaces
    $css = preg_replace('/\p{Zs}+/ims',' ', $css);
    // remove newlines
    $css = str_replace(array("\r\n", "\r", "\n"), '', $css);
    // Restore backupped values within single or double quotes
    for ($i=0; $i < count($hit[1]); $i++) {
      $css = str_replace('##########' . $i . '##########', $hit[1][$i], $css);
    }
    return $css;
}

/**
 * Cafetora Custom fonts
 */
function cafetora_custom_fonts( $fonts ){
    global $ct_option;
    
    $fonts = array();

    $heading_font = ( $ct_option['heading_font'] != '' ) ? $ct_option['heading_font'] : 'Alegreya';
    $text_font = ( $ct_option['text_font'] != '' ) ? $ct_option['text_font'] : 'Lora';

    $fonts[] = $heading_font;
    $fonts[] = $text_font;

    return $fonts;
}
add_filter('cafetora_font_families','cafetora_custom_fonts');

/**
 * Update Elementor Options
 */
function cafetora_load_elementor_options() {  
    update_option( 'elementor_disable_color_schemes', 'yes' );
    update_option( 'elementor_disable_typography_schemes', 'yes' );
    update_option( 'elementor_global_image_lightbox', '' );
}
add_action( 'elementor/loaded', 'cafetora_load_elementor_options' );

/**
 * Disable Getting Start - Elementor 
 */
function ct_elementor_loaded_function() {
    if ( did_action( 'elementor/loaded' ) ) {
        remove_action( 'admin_init', [ \Elementor\Plugin::$instance->admin, 'maybe_redirect_to_getting_started' ] );
    }
};
add_action( 'admin_init', 'ct_elementor_loaded_function', 1 );

/**
 * Cafetora admin css
 */
function cafetora_admin_css(){
    $css = '<style type="text/css">';
    $css .= '
    .clearfix {
        clear: both !important;
    }
    .acf-field .acf-label label {
        font-weight: 500;
    }
    .acf-field[data-width]+.acf-field[data-width] {
        border: 0;
    }
    .admin-color-fresh .redux-sidebar .redux-group-menu li.active.hasSubSections ul.subsection li a span.group_title, .admin-color-fresh .redux-sidebar .redux-group-menu li.activeChild.hasSubSections ul.subsection li a span.group_title, .wp-customizer .redux-sidebar .redux-group-menu li.active.hasSubSections ul.subsection li a span.group_title, .wp-customizer .redux-sidebar .redux-group-menu li.activeChild.hasSubSections ul.subsection li a span.group_title {
        padding-left: 30px !important;
    }
    ';

    $css .= '</style>';

    printf( '%s', $css );
}
add_action('admin_head', 'cafetora_admin_css', 200);