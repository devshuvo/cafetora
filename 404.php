<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package Cafetora
 */
get_header();
?>
    <!-- Blog Area Start -->
    <div class="cafetora-content-block section-padding">
        <div class="container">
           <div class="row">
              <div class="col-md-12">
                  <div class="section-heading">
                  	<h1><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'cafetora' ); ?></h1>
                    <div class="section-border"></div>
                  </div>
              </div>
             </div>
             
             <div class="error-404 not-found">
                <div class="loading-content">
                    <div class="row">
						<div class="col-md-12 text-center">
							<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'cafetora' ); ?></p>
							<p><?php get_search_form(); ?></p>
						</div>
                    </div>
                </div>
            </div>

         </div>
    </div>
    <!-- Blog Area End -->

<?php get_footer();
