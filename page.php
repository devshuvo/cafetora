<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @package Cafetora
 */
get_header();

global $ct_option;

$container_col = 'col-md-12';
if ( is_active_sidebar( 'sidebar-1' ) && $ct_option['page_sidebar'] != '1' && class_exists( 'ReduxFramework' )) {
	$container_col = 'col-md-8 sidebar-active';
}

?>
<!-- Single Page Area Start -->
    <div class="cafetora-content-block section-padding">
        <div class="container">
           <div class="row">
           		<?php 
                if( $ct_option['page_sidebar'] == '2' )
                	get_sidebar(); 
                ?>
                <div class="<?php echo esc_attr( $container_col ); ?>">
					<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content', get_post_type() );

						the_post_navigation();

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>
                </div>
                <?php 
                if( $ct_option['page_sidebar'] == '3' )
                	get_sidebar(); 
                ?>
            </div>
        </div>
    </div>
<?php
get_footer();