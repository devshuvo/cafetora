<?php
/**
 * Template part for search content
 *
 * @package Cafetora
 */

?>

<div class="col-md-4">
    <a href="<?php echo esc_url( get_permalink() ); ?>" id="post-<?php the_ID(); ?>" <?php post_class('single-blog'); ?>>
        <div class="blog-preview">
        	<?php the_post_thumbnail( 'blog-thumb' ); ?>
        </div>
        <h3><?php the_title(); ?></h3>
        <?php cafetora_post_meta(); ?>
        <p><?php echo wp_trim_words( get_the_content(), 25); ?></p>
    </a>
</div>
