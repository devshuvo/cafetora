<?php
/**
 * Template part for displaying page content in page.php
 *
 * @package Cafetora
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?>>
    <div class="post-title-wrap text-center">
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
    </div>
    <?php if ( has_post_thumbnail() ) : ?>
	    <div class="featured-image fullwidth">
	        <?php the_post_thumbnail(); ?>
	    </div>
	<?php endif; ?>
    <div class="entry-content">
        <?php 
        the_content();
        wp_link_pages(
            array(
                'before' => '<div class="page-links">' . __( 'Pages:', 'cafetora' ),
                'after'  => '</div>',
            )
        );
        ?>
    </div>
</article>