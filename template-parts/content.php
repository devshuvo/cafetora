<?php
/**
 * Template part for displaying posts
 *
 * @package Cafetora
 */

?>

<?php if ( is_singular() ) : ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?>>
        <div class="post-title-wrap text-center">
            <h1><?php the_title(); ?></h1>
            <?php cafetora_post_meta(); ?>
        </div>
        <div class="entry-content">
        	<?php if ( has_post_thumbnail() ) : ?>
    	        <div class="featured-image">
    	            <?php the_post_thumbnail(); ?>
    	        </div>
    	    <?php endif; ?>
            <?php 
            the_content();
            wp_link_pages(
                array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'cafetora' ),
                    'after'  => '</div>',
                )
            );
            ?>
        </div>
    </article>
<?php else : ?>
    <div class="col-md-4">
        <a href="<?php echo esc_url( get_permalink() ); ?>" id="post-<?php the_ID(); ?>" <?php post_class('single-blog'); ?>>
            <?php if ( has_post_thumbnail() ) : ?>
                <div class="blog-preview">
                	<?php the_post_thumbnail( 'blog-thumb' ); ?>
                </div>
            <?php endif; ?>
            <h3><?php the_title(); ?></h3>
            <?php cafetora_post_meta(); ?>
            <p><?php echo wp_trim_words( get_the_content(), 25); ?></p>
        </a>
    </div>
<?php endif; ?>