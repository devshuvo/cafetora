<?php
/**
 * The template for displaying archive pages
 *
 * @package Cafetora
 */

get_header();
?>

    <div class="cafetora-content-block section-padding">
        <div class="container">
           <div class="row">
              <div class="col-md-12">
                  <div class="section-heading">
                  	<h1>
                  	<?php the_archive_title(); ?>
					</h1>
					<?php the_archive_description( '<div class="archive-description">', '</div>' ); ?>
                    <div class="section-border"></div>
                  </div>
              </div>
             </div>
             
             <div class="load-more-container wow fadeInLeft">
                <div class="loading-content">
                    <div class="row">
					<?php
					if ( have_posts() ) :

						/* Start the Loop */
						while ( have_posts() ) :
							the_post();

							/*
							 * Include the Post-Type-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', get_post_type() );

						endwhile;						

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>                        
                    </div>
                    <div class="row">
                    	<?php the_posts_navigation(); ?>
                    </div>
                </div>
            </div>

         </div>
    </div>
    <!-- Blog Area End -->

<?php get_footer();