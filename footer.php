<?php
/**
 * The template for displaying the footer
 *
 * @package Cafetora
 */

?>

    <?php do_action('cafetora_footer'); ?>

<?php wp_footer(); ?>
</body>
</html>