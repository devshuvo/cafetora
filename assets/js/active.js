(function ($) {
	"use strict";

    $(document).ready(function() {
        
        // Our Menu Sliding Tab
        $('.menu-slider').owlCarousel({
            items: 1,
            loop:0,
            margin:10,
            nav:false,
            dots:true,
            autoplay: true,
            mouseDrag: false,
            touchDrag: false,
            dotsContainer: '.dotsCont'
        });

        $('.dotsCont .owl-dot').click(function () {
          $('.menu-slider').trigger('to.owl.carousel', [$(this).index(), 300]);
        });

        // Testimonial Slider
        $(".testimonial-slides").owlCarousel({
            items: 1,
            nav: false,
            dots: true,
            autoplay: true,
            animateOut: 'slideOutDown',
            animateIn: 'flipInX',
            loop: true,
            navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            mouseDrag: false,
            touchDrag: false,
        });

        // Popup Video
        $('.ct-video-popup').magnificPopup({
            //disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
        
        // Responsive Menu
        $("ul#navigation").slicknav({
            prependTo: ".responsive-menu-wrap"
        });
        
        // Section Animations
        new WOW().init();

        // Show or hide the sticky footer button
        $(window).on( "scroll", function() {
            if ($(this).scrollTop() > 200) {
                $('.go-top').fadeIn(200);
            } else {
                $('.go-top').fadeOut(200);
            }
        });

        var scroll_top = $('.go-top');

        scroll_top.on('click', function(e){
            e.preventDefault();
            $('body,html').animate({
                scrollTop: 0
            }, 600);
        });
        
        // Custom Scroll Spy
        $(window).on('scroll', function() {
            $('section').each(function() {
                if($(window).scrollTop() >= $(this).offset().top-140) {
                    var id = $(this).attr('id');
                    if( typeof id !== 'undefined'  ){
                        $('.main-menu li a').parent('li').removeClass('active');
                        $('.main-menu li a[href="#'+ id +'"]').parent('li').addClass('active');
                    }
                }
            });
        });    
     }); //.document/ready

    $(window).on('load', function(){
        
        jQuery(".cafetora-site-preloader-wrap").fadeOut(500);

        // Wrap every letter in a span
        anime.timeline({loop: false})
       .add({
         targets: '.intro-heading .line',
         opacity: [0.5,1],
         scaleX: [0, 1],
         easing: "easeInOutExpo",
         duration: 900
       }).add({
         targets: '.intro-heading .line',
         duration: 600,
         easing: "easeOutExpo",
         translateY: function(e, i, l) {
           var offset = -0.625 + 0.625*2*i;
           return offset + "em";
         }
       }).add({
         targets: '.intro-heading .ampersand',
         opacity: [0,1],
         scaleY: [0.5, 1],
         easing: "easeOutExpo",
         duration: 900,
         offset: '-=600'
       }).add({
         targets: '.intro-heading .letters-left',
         opacity: [0,1],
         translateX: ["0.5em", 0],
         easing: "easeOutExpo",
         duration: 900,
         offset: '-=300'
       }).add({
         targets: '.intro-heading .letters-right',
         opacity: [0,1],
         translateX: ["-0.5em", 0],
         easing: "easeOutExpo",
         duration: 900,
         offset: '-=600'
       }).add({
         targets: '.intro-heading',
         opacity: 1,
         duration: 1000,
         easing: "easeOutExpo",
         delay: 1000
       });

       $(".testimonial-slides").trigger('refresh.owl');

    });

}(jQuery));