<?php
/**
 * Template Name: Page Builder Full Width
 *
 * The template for the page builder full-width.
 *
 * It contains header, footer and 100% content width.
 *
 * @package Cafetora
 */
get_header();
?>
<div class="cafetora-content-block">
    <div class="pagebuilder-section">
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) :
				the_post();
				the_content();
			endwhile;
		endif;
		?>
    </div>
</div>
<?php
get_footer();