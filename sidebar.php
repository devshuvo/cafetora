<?php
/**
 * The sidebar containing the main widget area
 *
 * @package Cafetora
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<!-- Right Sidebar Area Start -->
<div class="col-md-4">
    <div class="sidebar widget-area">
       <?php dynamic_sidebar( 'sidebar-1' ); ?>
    </div>
</div>
<!-- Right Sidebar Area End -->