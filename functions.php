<?php
/**
 * Cafetora functions
 *
 * @package Cafetora
 */

if ( ! function_exists( 'cafetora_after_setup_theme' ) ) :
	function cafetora_after_setup_theme() {
		// Load Translation
		load_theme_textdomain( 'cafetora', get_template_directory() . '/lang' );

		// Theme Supports
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		add_theme_support( 'custom-logo', array(
			'height'      => 85,
			'width'       => 270,
			'flex-width'  => true,
			'flex-height' => true,
		) );		
		add_theme_support( 'custom-background', apply_filters( 'cafetora_custom_background', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Register Nav Menus
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'cafetora' ),
		) );

		// Custom image sizes
		add_image_size( 'blog-thumb', 320, 220, true );
		add_image_size( 'gallery-thumb', 338, 225, true );
		add_image_size( 'team-image', 320, 320, true );
		add_image_size( 'testimonial-image', 160, 160, true );
	}
endif;
add_action( 'after_setup_theme', 'cafetora_after_setup_theme' );

/**
 * Set Content Width
 */
function cafetora_content_width() {	
	$GLOBALS['content_width'] = apply_filters( 'cafetora_content_width', 640 );
}
add_action( 'after_setup_theme', 'cafetora_content_width', 0 );

/**
 * Register widges
 */
function cafetora_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'cafetora' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'cafetora' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget One', 'cafetora' ),
		'id'            => 'footer-widget-1',
		'description'   => esc_html__( 'Add widgets here.', 'cafetora' ),
		'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widget-title">',
		'after_title'   => '</h5>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Two', 'cafetora' ),
		'id'            => 'footer-widget-2',
		'description'   => esc_html__( 'Add widgets here.', 'cafetora' ),
		'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widget-title">',
		'after_title'   => '</h5>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Three', 'cafetora' ),
		'id'            => 'footer-widget-3',
		'description'   => esc_html__( 'Add widgets here.', 'cafetora' ),
		'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widget-title">',
		'after_title'   => '</h5>',
	) );
}
add_action( 'widgets_init', 'cafetora_widgets_init' );

/**
 * Register custom fonts.
 */
function cafetora_fonts_url() {
	$fonts_url = '';
		
	$fonts = array();
	$font_families = array();

	$weights = apply_filters( 'cafetora_font_weights', array( '400', '500', '700', '700i' ) );
	$fonts = apply_filters( 'cafetora_font_families', $fonts );

	foreach ( $fonts as $font ) {
		$font_families[] = $font . ':' . implode( ',', $weights );
	}

	$query_args = array(
		'family' => urlencode( implode( '|', $font_families ) )
	);

	$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	
	return esc_url_raw( $fonts_url );
}

/**
 * Enqueue scripts and styles.
 */
function cafetora_load_scripts() {
    global $ct_option;
       
	$version = wp_get_theme()->get( 'Version' );

	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css');
	wp_enqueue_style('animate', get_template_directory_uri() . '/assets/css/animate.css');
	wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css');
	wp_enqueue_style('owl-theme', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css');
	wp_enqueue_style('magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css');
	wp_enqueue_style('slicknav', get_template_directory_uri() . '/assets/css/slicknav.min.css');

	if ( did_action( 'elementor/loaded' ) ) {
		wp_enqueue_style( 'cafetora-style', get_stylesheet_uri(), array('elementor-frontend'), $version );
	}else{
		wp_enqueue_style( 'cafetora-style', get_stylesheet_uri(), '', $version );
	}

	// Enqueue fonts
	wp_enqueue_style( 'cafetora_fonts', cafetora_fonts_url(), array(), '1.0.0' );	

	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('slicknav', get_template_directory_uri() . '/assets/js/jquery.slicknav.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('wow', get_template_directory_uri() . '/assets/js/wow.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('anime', get_template_directory_uri() . '/assets/js/anime.min.js', array( 'jquery' ), '', true);
	if ( $ct_option['sticky_header'] == 1 ){
		wp_enqueue_script('sticky-header', get_template_directory_uri() . '/assets/js/sticky-header.js', array( 'jquery' ), '', true);
	}
	wp_enqueue_script( 'cafetora-script', get_template_directory_uri() . '/assets/js/active.js', array(), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'cafetora_load_scripts' );

/**
 * Functions to enhance cafetora
 */
require dirname( __FILE__ ) . '/inc/template-functions.php';

/**
 * TGM Activation
 */
require dirname( __FILE__ ) . '/inc/tgm/tgm-init.php';

/**
 * Google Font list 
 */
require dirname( __FILE__ ) . '/inc/fonts.php';

/**
 * Hide the ACF admin menu item.
 */
function cafetora_acf_settings_show_admin( $show_admin ) {
    return false;
}
add_filter('acf/settings/show_admin', 'cafetora_acf_settings_show_admin');

/**
 * Acf Exported Field
 */
require dirname( __FILE__ ) . '/inc/acf-fields.php';

/**
 * Demo Importer inital
 */
if( class_exists( 'OCDI_Plugin' ) ){
	require dirname( __FILE__ ) . '/inc/demo-content.php';
}

/**
 * Redux Options Panel
 */
if ( class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/inc/options-init.php' ) ) {
    require_once dirname( __FILE__ ) . '/inc/options-init.php';
}

/**
 * Custom CSS
 */
require dirname( __FILE__ ) . '/inc/custom-css.php';
