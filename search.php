<?php
/**
 * The template for displaying search results pages
 *
 * @package Cafetora
 */
get_header();
?>
    <!-- Blog Area Start -->
    <div class="cafetora-content-block section-padding">
        <div class="container">
           <div class="row">
              <div class="col-md-12">
                  <div class="section-heading">
                  	<h1>
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'cafetora' ), '<span>' . get_search_query() . '</span>' );
					?>					
					</h1>
                    <div class="section-border"></div>
                  </div>
              </div>
             </div>
             
             <div class="load-more-container wow fadeInLeft">
                <div class="loading-content">
                    <div class="row">
					<?php
					if ( have_posts() ) :

						/* Start the Loop */
						while ( have_posts() ) :
							the_post();

							/**
							 * Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called content-search.php and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'search' );

						endwhile;						

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>                        
                    </div>
                    <div class="row">
                    	<?php the_posts_navigation(); ?>
                    </div>
                </div>
            </div>

         </div>
    </div>
    <!-- Blog Area End -->

<?php get_footer();