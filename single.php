<?php
/**
 * The template for displaying all single posts
 *
 * @package Cafetora
 */

get_header();

global $ct_option;

if ( class_exists('ACF') ){
    $select_sidebar = get_field('select_sidebar');
} else {
    $select_sidebar = '0';
}

if ( class_exists( 'ReduxFramework' ) && isset( $ct_option['post_sidebar'] ) ){
    if ( $select_sidebar == '0' ) {
        $post_sidebar = $ct_option['post_sidebar'];
    } else{
        $post_sidebar = $select_sidebar;
    }
} elseif ( class_exists('ACF') && $select_sidebar != '0' ) {
    $post_sidebar = $select_sidebar;
} else {
    $post_sidebar = 'default';
}

$container_col = 'col-md-12 fullwidth-img';
if ( is_active_sidebar( 'sidebar-1' ) && $post_sidebar != '1' ) {
    $container_col = 'col-md-8 sidebar-active';
}

?>
<!-- Single Post Area Start -->
    <div class="cafetora-content-block section-padding">
        <div class="container">
           <div class="row">
                <?php 
                if( $post_sidebar == '2' ){
                	get_sidebar(); 
                }
                ?>
                <div class="<?php echo esc_attr( $container_col ); ?>">
					<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content', get_post_type() );

						the_post_navigation();

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>
                </div>
                <?php 
                
	            if ( $post_sidebar == '3' || $post_sidebar == 'default' ) {
	                get_sidebar();
                }
                ?>
            </div>
        </div>
    </div>
    <!-- Single Post Area End -->
<?php
get_footer();
